export const environment = {
  production: true,
  endPoint: 'https://asisted.innovated.xyz/',
  imgNotFound: 'assets/img/confi/usernot.svg',
  imgNotFoundCompany: 'assets/img/inicio/ide.png'
};
