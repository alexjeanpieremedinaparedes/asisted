import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { NavbarwebComponent } from './navbarweb/navbarweb.component';
import { NavbaradminComponent } from './navbaradmin/navbaradmin.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FooterinicioComponent } from './footerinicio/footerinicio.component';
import { RouterModule } from '@angular/router';
import { DemoMaterialModule } from '../materia-module';
import { DirectiveModule } from '../directive/directive.module';




@NgModule({
  declarations: [

    FooterComponent,
    NavbarwebComponent,
    NavbaradminComponent,
    FooterinicioComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    RouterModule,
    DemoMaterialModule,
    DirectiveModule

  ],
  exports: [
    NavbarwebComponent,
    NavbaradminComponent,
    FooterinicioComponent

  ]
})
export class SharedModule { }
