import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginAdminComponent } from 'src/app/auth/login-admin/login-admin.component';
import { CerrarseccionComponent } from 'src/app/components/cerrarseccion/cerrarseccion.component';
import { AuthService } from 'src/app/service/auth.service';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-navbarweb',
  templateUrl: './navbarweb.component.html',
  styleUrls: ['./navbarweb.component.css']
})
export class NavbarwebComponent implements OnInit {
  AttendanceList:any = [];
  menutoogle: boolean = false;
  data:any;
  openTabUsers = 1;
  constructor(
    public modal: MatDialog,
    private sfn: FunctionService,
    private suth: AuthService)
  {
    this.data = this.sfn.LocalStorageSesionBusiness();
  }

  ngOnInit(): void { }

  tabsUsers($tabNumber: number) {
    this.openTabUsers = $tabNumber;
  }

  loginAdmin() {
    this.modal.open(LoginAdminComponent);

  }

  async logoutWeb() {
    const head = 'Web';
    const exito = await this.sfn.logoutQuestion(head);

    if (exito) {
      this.suth.logout();
    }
  }
}
