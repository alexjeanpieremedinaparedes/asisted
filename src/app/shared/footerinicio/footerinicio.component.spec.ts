import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterinicioComponent } from './footerinicio.component';

describe('FooterinicioComponent', () => {
  let component: FooterinicioComponent;
  let fixture: ComponentFixture<FooterinicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterinicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterinicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
