import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { IauthResult } from 'src/app/interface/auth.interface';
import { Ilogin } from 'src/app/interface/user.interface';
import { EnvSystem } from 'src/app/model/env.model';
import { FunctionService } from 'src/app/service/function.service';
import { UserService } from 'src/app/service/user.service';
import { ConfiguracionesComponent } from '../../pages/admin/configuraciones/configuraciones.component';

@Component({
  selector: 'app-navbaradmin',
  templateUrl: './navbaradmin.component.html',
  styleUrls: ['./navbaradmin.component.css']
})
export class NavbaradminComponent implements OnInit {

  company!: IauthResult;
  user!: Ilogin;

  openTab1: boolean = false;
  openTab2: boolean = false;
  openTab3: boolean = false;
  menutoogle : boolean = false;

  private env = new EnvSystem();

  constructor(
    private router: Router,
    public modals:MatDialog,
    public sfn: FunctionService,
    private sAuthAdmin: UserService) {

    this.loadDataDefult();
    const ruta = this.router.url;

    if (ruta.includes('dashboard')) this.openTab1 = true;
    else if (ruta.includes('gestionusuario')) this.openTab2 = true;
    else if (ruta.includes('habitaciones')) this.openTab3 = true;
  }

  ngOnInit(): void { }

  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
    this.openTab3 = false;
  }

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber == 2) this.openTab2 = true;
    else if ($tabNumber == 3) this.openTab3 = true;    
  }
  
  configuracion(){
    this.modals.open(ConfiguracionesComponent,{
      panelClass: 'full-screen-modal',
    });
  }

  loadDataDefult() {
    this.company = this.sfn.decrypt(this.env.dataCompany);
    this.user = this.sfn.decrypt(this.env.dataUser);
  }

  async logoutAdmin() {
    const head = 'Admin';
    const exito = await this.sfn.logoutQuestion(head);

    if (exito) {
      this.sAuthAdmin.logoutAdmin();
    }
  }


}
