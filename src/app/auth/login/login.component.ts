import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EnvSystem } from 'src/app/model/env.model';
import { AuthService } from 'src/app/service/auth.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidationService } from 'src/app/service/validation.service';
import { ValidatosComponent } from '../modales/validatos/validatos.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  //messages
  message?: string;
  succes?: string;
  error?: boolean;

  //Formulario
  form!: FormGroup;

  //Entorno
  env = new EnvSystem();

  constructor(
    private modal: MatDialog,
    private fb: FormBuilder,
    private sval: ValidationService,
    private spinner: NgxSpinnerService,
    private sauth: AuthService,
    private router: Router,
    private fn: FunctionService
  ) {
    if(fn.LocalStorageSesionBusiness()) this.next();
    this.createForm();
  }

  ngOnInit(): void {

  }

  get rucInvalid() {
    return this.sval.ctrInvalid('ruc', this.form);
  }

  get codeInvalid() {
    return this.sval.ctrInvalid('codigoacceso', this.form);
  }

  createForm() {
    this.form = this.fb.group({
      ruc: [ '', [ Validators.required ] ] ,
      codigoacceso: [ '', [ Validators.required ] ]
    });
  }

  enterOkay() {

    if( this.form.invalid ) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };
    this.sauth.login( body ).subscribe( response => {

      const ok = response.message = 'exito';
      if( ok ) {

        const data = JSON.stringify( response.result );
        localStorage.setItem(this.env.dataCompany, this.fn.EncryptData(data,true));
        this.next();
      }

      this.spinner.hide();
    }, (e) => this.exception(e) );

  }
  next() {
    this.router.navigateByUrl('/web/inicio', { replaceUrl: true });
  }

  //ENTORNO
  exception(e: any) {
    this.error = true;
    this.message = (e.error.message) ?? this.env.msjBrokenConnection;
    this.spinner.hide();
    this.onValiDatos();
  }

  //MODALS

  onValiDatos(){
    this.modal.open(ValidatosComponent,{data:{msj:this.message}})
  }


}
