import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EnvSystem } from 'src/app/model/env.model';
import { FunctionService } from 'src/app/service/function.service';
import { UserService } from 'src/app/service/user.service';
import { ValidationService } from 'src/app/service/validation.service';
import { ValidatosComponent } from '../modales/validatos/validatos.component';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

  //messages
  message?: string;
  succes?: string;
  error?: boolean;

  //Formulario
  form!: FormGroup;
  env = new EnvSystem();

  constructor(
    private user:UserService,
    private fb: FormBuilder,
    private sval: ValidationService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private fn: FunctionService,
    private modal: MatDialog
  ) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm() {
    this.form = this.fb.group({
      usuario: [ '', [ Validators.required ] ] ,
      password: [ '', [ Validators.required ] ]
    });
  }
  get userInvalid() {
    return this.sval.ctrInvalid('usuario', this.form);
  }
  get passwordInvalid() {
    return this.sval.ctrInvalid('password', this.form);
  }

  Login()
  {
    if( this.form.invalid ) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };
    this.user.LoginUser( body ).subscribe( response => {

      console.log(response);

      const ok = response.message = 'exito';
      if( ok ) {
        const data = JSON.stringify( response.result );
        localStorage.setItem(this.env.dataUser,this.fn.EncryptData(data,true));
        this.router.navigateByUrl('/admin/dashboard', { replaceUrl: true });
      }

      this.spinner.hide();
      this.modal.closeAll();
    }, (e) => this.exception(e) );
  }

  exception(e: any) {
    this.error = true;
    this.message = (e.error.message) ?? this.env.msjBrokenConnection;
    this.spinner.hide();
    this.onValiDatos();
  }

  onValiDatos(){
    this.modal.open(ValidatosComponent,{data:{msj:this.message}})
  }
}
