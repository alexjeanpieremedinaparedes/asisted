import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-validatos',
  templateUrl: './validatos.component.html',
  styles: [
  ]
})

export class ValidatosComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private sauth: AuthService ) {}

  ngOnInit(): void { }

  click() {
    if( this.data.expired ) {
      this.sauth.logout();
    }
  }
}
