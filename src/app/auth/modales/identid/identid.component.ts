import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-identid',
  templateUrl: './identid.component.html',
  styleUrls: ['./indentidad.component.css']

})
export class IdentidComponent implements OnInit {

  openTabUsers = 1;
  constructor() { }

  ngOnInit(): void {
  }

  tabsUsers($tabNumber: number) {
    this.openTabUsers = $tabNumber;
  }
}
