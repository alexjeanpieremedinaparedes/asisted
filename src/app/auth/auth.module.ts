import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../materia-module';


import { IdentidComponent } from './modales/identid/identid.component';
import { ValidatosComponent } from './modales/validatos/validatos.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { LoginAdminComponent } from './login-admin/login-admin.component';






@NgModule({
  declarations: [
    LoginComponent,
    IdentidComponent,
    ValidatosComponent,
    LoginAdminComponent

  ],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
  ]
})
export class AuthModule { }
