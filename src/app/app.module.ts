import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './materia-module';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { AdminModule } from './pages/admin/admin.module';
import { WebModule } from './pages/web/web.module';
import { ComponentsModule } from './components/components.module';
import { NgxSpinnerModule } from "ngx-spinner";

import { CommonModule, registerLocaleData } from '@angular/common';
import LocaleEsPe from '@angular/common/locales/es-PE.js';
import { MAT_DATE_LOCALE } from '@angular/material/core'; // Calendario En español
registerLocaleData(LocaleEsPe);

import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DemoMaterialModule,
    SharedModule,
    AuthModule,
    AdminModule,
    ComponentsModule,
    WebModule,
    HttpClientModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    NgxSpinnerModule,
    CommonModule
  ],
  
  providers: [
    { 
      provide: MAT_DATE_LOCALE,
      useValue: 'es-PE'
    },
    {
      provide: LOCALE_ID,
      useValue: 'es-PE'
    },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
