import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Iauth } from '../interface/auth.interface';
import { EnvSystem } from '../model/env.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url?: string;
  private env = new EnvSystem();

  constructor(
    private http: HttpClient,
    private router: Router ) {
    this.url = 'empresa/';
  }

  login( body: any ) : Observable<Iauth> {
    return this.http.post<Iauth>(`${this.env.urlRaiz}${this.url}iniciosesionempresa`, body );
  }

  logout() {
    
    localStorage.removeItem('_dataTokenAsistedUser');
    localStorage.removeItem('_dataTokenAsistedCompany');
    this.router.navigateByUrl('/login', { replaceUrl: true })
  }
}
