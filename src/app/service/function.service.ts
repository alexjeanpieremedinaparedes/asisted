import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import * as CryptoJS  from 'crypto-js';
import { ValidatosComponent } from '../auth/modales/validatos/validatos.component';
import { CerrarseccionComponent } from '../components/cerrarseccion/cerrarseccion.component';
import { ModalsdeleteComponent } from '../components/modalsdelete/modalsdelete.component';
import { NotificacionComponent } from '../components/notificacion/notificacion.component';
import { EnvSystem } from '../model/env.model';
import { HttpClient } from '@angular/common/http';

import *  as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXCEL_EXT = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  env = new EnvSystem();
  constructor(
    private router: Router,
    private modal: MatDialog,
    private _snackBar: MatSnackBar,
    private http:HttpClient
  ) { }

  public exportToExcel(json: any, excelFileName: string): void {

    const workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workBook: XLSX.WorkBook = { Sheets: { 'data': workSheet }, SheetNames: ['data'] };
    const excelbuffer: any = XLSX.write(workBook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcel(excelbuffer, excelFileName);

  }

  private saveAsExcel(buffer: any, FileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, FileName + EXCEL_EXT);
  }

  EncryptData(data:string,encrypt:boolean) {
    return encrypt ? CryptoJS.AES.encrypt(data,this.env.KeyLocalStorage).toString()
                   : CryptoJS.AES.decrypt(data, this.env.KeyLocalStorage).toString(CryptoJS.enc.Utf8);
  }

  decrypt( local: string ) {
    const encrypt:any = localStorage.getItem(local);
    if(encrypt)
    {
      const decrypt:string = this.EncryptData(encrypt,false);
      return JSON.parse(decrypt);
    }
  }

  LocalStorageSesionBusiness() {
    const encrypt:any = localStorage.getItem(this.env.dataCompany);
    if(encrypt)
    {
      const decrypt:string = this.EncryptData(encrypt,false);
      return JSON.parse(decrypt);
    }
    else this.router.navigateByUrl('/login');
  }

  LocalStorageSesionUser() {
    const businessencrypt:any = localStorage.getItem(this.env.dataCompany);
    const encrypt:any = localStorage.getItem(this.env.dataUser);
    if(encrypt && businessencrypt)
    {
      const decrypt:string = this.EncryptData(encrypt,false);
      return JSON.parse(decrypt);
    }
    else this.router.navigateByUrl('/web/inicio');
  }

  headersApi(tkuser:boolean = false): any {
    const token = this.LocalStorageSesionBusiness().token;
    return tkuser ? { 'Authorization': `bearer ${token}`,'Authorization2': `bearer ${token}` } : { 'Authorization': `bearer ${token}` }
  }

  convertbs64ForSrc(bs64img: any): string {
    return 'data:image/jpeg;base64,' + bs64img;
  }

  groupByKey(array: any[], key: string) {
    return array
      .reduce((hash, obj) => {
        if(obj[key] === undefined) return hash;
        return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)})
      }, {})
  }

  async modalQuestion( head: string, mjs: string ): Promise<boolean> {

    const body = {
      headboardMessage: head,
      message: mjs
    };

    return await new Promise( (resolve, reject) =>{

      this.modal.open(ModalsdeleteComponent, {
        data: body,
        disableClose: true
      }).afterClosed().subscribe( exito => {
        resolve(exito);
      })
    });
  }

  async logoutQuestion( head: string ): Promise<boolean> {

    const body = {
      operacion: head,
    };

    return await new Promise( (resolve, reject) =>{

      this.modal.open(CerrarseccionComponent, {
        data: body,
        disableClose: true
      }).afterClosed().subscribe( exito => {
        resolve(exito);
      })
    });
  }

  exception(e: any){

    const expired = e.error === 'Unauthorized';
    const message = (expired) ? this.env.msjExpired : e.error.message ?? this.env.msjBrokenConnection;

    const body = {
      msj: message,
      expired: expired
    }

    this.modal.open(ValidatosComponent, {
      data: body,
      disableClose: true
    });
  }

  openSnackBar( msj: string ) {

    const horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    const verticalPosition: MatSnackBarVerticalPosition = 'top';
    const durationInSeconds = 5;

    this._snackBar.openFromComponent(NotificacionComponent, {
      duration: durationInSeconds * 200,
      horizontalPosition: horizontalPosition,
      verticalPosition: verticalPosition,
      data: msj
    });
  }

  getIP() {
    return this.http.get(`http://api.ipify.org/?format=json`);
  }
  getLocation() {
    return this.http.get(`http://ip-api.com/json/`);
  }

  public parseTimeString(value: string) {
    return new Date (new Date().toDateString() + ' ' + value);
  }

  public parseTimeInt( value: string ) {
    const regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
    return parseInt(value.replace(regExp, '$1$2$3' ));
  }

  public parseTime( value: string ): string {

    if(!value) return '';
    let time = value;
    const timeArray = value.split(':');
    if(timeArray.length < 3 && time.length > 1) {
      return time + ':00';
    }
    
    return time;
  }

  public onlyNumberAllowed(event: any, limit: number) : boolean {
    const charCode = (event.which)?event.which: event.keycode;
    if(charCode > 31 && (charCode < 48 || charCode > 57 )) {
      return false;
    }
  
    const len = event.target.value.length;
    if(len > (limit - 1)) return false;

    return true;
  }

}
