import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Iemployee, Iroles, Ihorarios, ItypeDocuento } from '../interface/employee.interface';
import { EnvSystem } from '../model/env.model';
import { FunctionService } from './function.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public newEmploye$ = new EventEmitter<boolean>();
  public newTimetable$ = new EventEmitter<boolean>();

  private url?: string;
  private env = new EnvSystem();

  constructor(
    private http: HttpClient,
    private fn: FunctionService
  ) {
    this.url = 'empleado/';
  }

  getEmployee() : Observable<Iemployee[]> {

    const headers = this.fn.headersApi(true);
    return this.http.get<Iemployee[]>(`${this.env.urlRaiz}${this.url}listarempleado`, { headers }).pipe( map( (result: any) => result.result ) );
  }

  deleteEmployee( body: any ) {

    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}eliminarempleado`, body, { headers });
  }

  deleteRoles( body: any ) {

    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}eliminarcargo`, body, { headers });
  }

  deletetimetable( body: any ) {

    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}eliminarhorario`, body, { headers });
  }

  getRoles(): Observable<Iroles[]> {

    const headers = this.fn.headersApi(true);
    return this.http.get<Iroles[]>(`${this.env.urlRaiz}${this.url}listarcargo`, { headers }).pipe( map( (result: any) => result.result ) );
  }

  getTimeTable(): Observable<Ihorarios[]> {

    const headers = this.fn.headersApi(true);
    return this.http.get<Ihorarios[]>(`${this.env.urlRaiz}${this.url}listarhorario`, { headers }).pipe( map( (result: any) => result.result ) );
  }

  getTypeDocument(): Observable<ItypeDocuento[]> {

    const headers = this.fn.headersApi(true);
    return this.http.get<ItypeDocuento[]>(`${this.env.urlRaiz}${this.url}listartipodocumento`, { headers }).pipe( map( (result: any) => result.result ) );
  }

  setEmployee( body: any ) {

    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}agregarempleado`, body, { headers });
  }

  setRole( body: any ) {

    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}agregarcargo`, body, { headers });
  }

  setTimeTable( body: any ) {
    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}agregarhorario`, body, { headers });
  }

  setTimeEmpleyee( body: any ) {
    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}asignarhorarioemeplado`, body, { headers });
  }

}
