import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Iattendance } from '../interface/attendance.interface';
import { EnvSystem } from '../model/env.model';
import { FunctionService } from './function.service';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  private url?: string;
  private env = new EnvSystem();

  constructor(
    private http: HttpClient,
    private fn: FunctionService
  ) {
    this.url = 'asistencia/';
  }

  DailyAssistance() : Observable<Iattendance> {
    const headers = this.fn.headersApi()
    return this.http.get<Iattendance>(`${this.env.urlRaiz}${this.url}listarasistenciadiaria`, { headers } );
  }

  Assistance(body:any){
    const headers = this.fn.headersApi()
    return this.http.post(`${this.env.urlRaiz}${this.url}marcarasistencia`, body ,{ headers } );
  }
}
