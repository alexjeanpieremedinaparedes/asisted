import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Iemployee } from '../interface/employee.interface';
import { Ireport } from '../interface/report.interface';
import { IuserLogin } from '../interface/user.interface';
import { EnvSystem } from '../model/env.model';
import { FunctionService } from './function.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private url?: string;
  private env = new EnvSystem();

  constructor(
    private http: HttpClient,
    private fn: FunctionService
  ) {
    this.url = 'reporte/';
  }

  AttendanceReportList(data:any) : Observable<Ireport> {
    const headers = this.fn.headersApi(true)
    return this.http.post<Ireport>(`${this.env.urlRaiz}${this.url}listarasistenciaempleado`,data, { headers });
  }

  getEmployee() : Observable<Iemployee[]> {
    const headers = this.fn.headersApi(true);
    return this.http.get<Iemployee[]>(`${this.env.urlRaiz}${this.url}listarempleado`, { headers }).pipe( map( (result: any) => result.result ) );
  }
}
