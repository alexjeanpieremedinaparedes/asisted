import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Ireniec } from '../interface/reniec.interface';
import { FunctionService } from './function.service';

@Injectable({
  providedIn: 'root'
})
export class OthersService {

  constructor( 
    private http: HttpClient,
    private fn: FunctionService
   ) { }

   getReniec(documento: string) : Observable<Ireniec> {

    const headers = this.fn.headersApi(true);
    return this.http.get<Ireniec>(`https://asisted.innovated.xyz/empleado/getreniec/${documento}`, { headers });
  }
}
