import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Iform, IuserList, IuserLogin } from '../interface/user.interface';
import { EnvSystem } from '../model/env.model';
import { FunctionService } from './function.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url?: string;
  private env = new EnvSystem();

  constructor(
    private http: HttpClient,
    private fn: FunctionService,
    private router: Router
  ) {
    this.url = 'usuario/';
  }

  LoginUser(data:any) : Observable<IuserLogin> {
    const headers = this.fn.headersApi();
    return this.http.post<IuserLogin>(`${this.env.urlRaiz}${this.url}iniciosesionusuario`,data, { headers });
  }

  logoutAdmin() {
    localStorage.removeItem('_dataTokenAsistedUser');
    this.router.navigateByUrl('/web/inicio', { replaceUrl: true });
  }

  getUser()
  {
    const headers = this.fn.headersApi(true);
    return this.http.get<IuserList>(`${this.env.urlRaiz}${this.url}listarusuario`, { headers });
  }

  getForm()
  {
    const headers = this.fn.headersApi(true);
    return this.http.get<Iform>(`${this.env.urlRaiz}${this.url}listarformulario`, { headers });
  }

  setUser(body:any)
  {
    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}agregarusuario`, body ,{ headers });
  }

  deleteUser(body:any)
  {
    const headers = this.fn.headersApi(true);
    return this.http.post(`${this.env.urlRaiz}${this.url}eliminarusuario`, body ,{ headers });
  }
}
