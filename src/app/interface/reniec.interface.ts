export interface Ireniec {
    status:  string;
    message: string;
    result:  Ireniecresult;
}

export interface Ireniecresult {
    nuDni:               string;
    apePaterno:          string;
    apeMaterno:          string;
    preNombres:          string;
    digitoVerificacion:  string;
    nuImagen:            string;
    feNacimiento:        string;
    estatura:            string;
    sexo:                string;
    estadoCivil:         string;
    gradoInstruccion:    string;
    feEmision:           string;
    feInscripcion:       string;
    nomPadre:            string;
    nomMadre:            string;
    cancelacion:         string;
    departamento:        string;
    provincia:           string;
    distrito:            string;
    depaDireccion:       string;
    provDireccion:       string;
    distDireccion:       string;
    feFallecimiento:     string;
    depaFallecimiento:   string;
    provFallecimiento:   string;
    distFallecimiento:   string;
    feCaducidad:         string;
    donaOrganos:         string;
    observacion:         string;
    vinculoDeclarante:   string;
    nomDeclarante:       string;
    deRestriccion:       string;
    desDireccion:        string;
    imagenes:            Ireniecimgs;
    fecha_actualizacion: string;
    ubicacion: IreniecUbigeo;
    contacto :IreniecContacto;
}

export interface Ireniecimgs {
    foto:    string;
    huellai: string;
    huellad: string;
    firma:   string;
}


export interface IreniecContacto {
    telefono: string;
    email:    string;
}

export interface IreniecUbigeo {
    codigopostal: string;
    ubigeoreniec: string;
    ubigeoinei:   string;
}
