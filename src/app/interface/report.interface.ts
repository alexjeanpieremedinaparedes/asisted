export interface Ireport {
  status  : number;
  message : string;
  result  : IreportList;
}

export interface IreportList {
  dia             : number;
  turno           : number;
  fechaasistencia : number;
  horaasistencia  : string;
  horasalida      : string;
  tardanza        : string;
  tiempo          : string;
}
