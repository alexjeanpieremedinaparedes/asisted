export interface Iauth {
    status:  number;
    message: string;
    result:  IauthResult;
}

export interface IauthResult {
    idempresa:       number;
    ruc:             string;
    razonsocial:     string;
    nombrecomercial: string;
    CodigoAcceso:    string;
    token:           string;
    logo:            string;
}