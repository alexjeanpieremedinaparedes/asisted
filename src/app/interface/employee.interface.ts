
//EMPLEADO

export interface Iemployee {
  idempleado      :number;
  idtipodocumento :string;
  idcargo         :string;
  idhorario       :number,
  tipodocumento   :string;
  cargo           :string;
  horario         :string; 
  numerodocumento :string;
  empleado        :string;
  nombre          :string;
  apellidoparteno :string;
  apellidomaterno :string;
  fechanacimiento :string;
  fechainscripcion:string;
  fechacaducudad  :string;
  fechaemision    :string;
  codigoubigeo    :string;
  grupovotacion   :string;
  urlfoto         :string;
}

//CARGO
export interface Iroles {
  idcargo  : number;
  idempresa: number;
  cargo    : string;
}


//HORARIOS
export interface Ihorarios {
  idhorario: number;
  horario:   string;
  detalle:   IrolesDetalle1[];
}

export interface IrolesDetalle1 {
  idhorariodetalle: number;
  idhorario:        number;
  dia:              string;
  detalle:          IrolesDetalle2[];
}

export interface IrolesDetalle2 {
  idhorariodetallehora: number;
  idhorariodetalle:     number;
  turno:                string;
  dia:                  string;
  horainicio:           string;
  horasalida:           string;
}

//TIPO DOCUMENTOS
export interface ItypeDocuento {
  idtipodocumento: number;
  tipodocumento:   string;
}

