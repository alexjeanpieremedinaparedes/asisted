export interface Iattendance {
  status  : number;
  message : string;
  result  : IattendanceResult;
}

export interface IattendanceResult {
  idasistencia    : number;
  idempleado      : number;
  fechaasistencia : string;
  empleado        : string;
  horaasistencia  : string;
  horasalida      : string;
  tipo            : string;
}

