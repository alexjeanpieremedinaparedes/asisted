export interface IuserLogin {
  status  : number;
  message : string;
  result  : Ilogin;
}

export interface Ilogin {
  ruc       : number;
  idempresa : number;
  idusuario : number;
  nombres   : string;
  celular   : string;
  correo    : string;
  token     : string;
  urlfoto   : string;
  cargo     : string;
}


export interface IuserList {
  status  : number;
  message : string;
  result  : IListUser;
}

export interface IListUser {
  idusuario   : number;
  idempleado  : number;
  empleado    : string;
  usuario     : string;
  password    : string;
  permisos    : any;
}


export interface Iform {
  status  : number;
  message : string;
  result  : IListform;
}

export interface IListform {
  idformulario  : number;
  formulario    : number;
}
