import { environment } from "src/environments/environment";

export class EnvSystem {

    urlRaiz: string;
    dataCompany: string;
    dataUser: string;
    msjExpired: string;
    msjBrokenConnection: string;
    KeyLocalStorage: string;
    capacityMaxhorario: number;

    constructor() {
        this.urlRaiz = environment.endPoint;
        this.dataCompany = '_dataTokenAsistedCompany';
        this.dataUser = '_dataTokenAsistedUser';
        this.msjExpired = 'conexión expirada, vuelva a iniciar sesión';
        this.msjBrokenConnection = 'Sin conexión al servidor';
        this.KeyLocalStorage = 'YOc0emRqARMDXpkdw==='
        this.capacityMaxhorario = 3;
    }
}
