import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { adminRoutingModule } from './pages/admin/admin.routing';
import { webRoutingModule } from './pages/web/web.routing';

const routes: Routes = [

    {path:'login',component:LoginComponent},
    {path:'**',pathMatch:'full', redirectTo:'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true}),   
    adminRoutingModule,
    webRoutingModule // web routing
     

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
