import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotImgDirective } from './not-img.directive';
import { NotimgcompanyDirective } from './notimgcompany.directive';

@NgModule({
  declarations: [
    NotImgDirective,
    NotimgcompanyDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NotImgDirective,
    NotimgcompanyDirective
  ]
})
export class DirectiveModule { }
