import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalsdeleteComponent } from './modalsdelete/modalsdelete.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { DemoMaterialModule } from '../materia-module';
import { NotificacionComponent } from './notificacion/notificacion.component';
import { AsistenciaguardadaComponent } from './asistenciaguardada/asistenciaguardada.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { SpinnerComponent } from './spinner/spinner.component';
import { CerrarseccionComponent } from './cerrarseccion/cerrarseccion.component';



@NgModule({
  declarations: [
    ModalsdeleteComponent,
    NotificacionComponent,
    AsistenciaguardadaComponent,
    SpinnerComponent,
    CerrarseccionComponent
  ],

  exports:[
    SpinnerComponent,
    ModalsdeleteComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule, 
    DemoMaterialModule,
    AngularSvgIconModule.forRoot(),
    NgxSpinnerModule
  ]
})
export class ComponentsModule { }
