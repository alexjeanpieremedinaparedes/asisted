import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-asistenciaguardada',
  templateUrl: './asistenciaguardada.component.html',
  styleUrls: ['./asistenciaguardada.component.css']
})
export class AsistenciaguardadaComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit(): void { 
    this.spinner.show();

    setTimeout(() => {     
      this.spinner.hide();
    }, 5000);
  }

}
