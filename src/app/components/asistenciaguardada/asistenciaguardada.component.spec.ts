import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciaguardadaComponent } from './asistenciaguardada.component';

describe('AsistenciaguardadaComponent', () => {
  let component: AsistenciaguardadaComponent;
  let fixture: ComponentFixture<AsistenciaguardadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsistenciaguardadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenciaguardadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
