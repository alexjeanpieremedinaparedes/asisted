import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-cerrarseccion',
  templateUrl: './cerrarseccion.component.html',
  styleUrls: ['./cerrarseccion.component.css']
})
export class CerrarseccionComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<FunctionService>,
    @Inject( MAT_DIALOG_DATA ) public data: any ) { }

  ngOnInit(): void {
  }

  continuar() {
    this.dialogRef.close({ exito: true });
  }

}
