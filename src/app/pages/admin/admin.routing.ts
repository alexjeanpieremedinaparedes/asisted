import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { InicioComponent } from '../web/inicio/inicio.component';
import { DashbardComponent } from './personalOperativo/dashbard/dashbard.component';
import { ListausuariosComponent } from './gestionUsuarios/listausuarios/listausuarios.component';
import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';




const routes: Routes = [
   
    {
        path: 'admin', component:AdminComponent,
    
        children:[
          { path: 'dashboard', component: DashbardComponent},
          { path: 'gestionusuario', component: ListausuariosComponent},
          // { path: 'configuraciones', component: ConfiguracionesComponent},
          { path: '**', redirectTo:'dashboard', pathMatch: 'full'},
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class adminRoutingModule {}
