import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(
    private router: Router,
    private fn: FunctionService
  )
  {
    if(this.fn.LocalStorageSesionUser()) this.router.navigateByUrl('/admin/dashboard', { replaceUrl: true });
  }

  ngOnInit(): void {
  }

}
