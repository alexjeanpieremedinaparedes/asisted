import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AdminComponent } from './admin.component';
import { DashbardComponent, Opciones } from './personalOperativo/dashbard/dashbard.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { DemoMaterialModule } from 'src/app/materia-module';
import { NuevopersonalComponent } from './personalOperativo/nuevopersonal/nuevopersonal.component';
import { NuevocargoComponent } from './personalOperativo/nuevocargo/nuevocargo.component';
import { HorarioComponent } from './personalOperativo/horario/horario.component';
import { ReporteasistenciaComponent } from './personalOperativo/reporteasistencia/reporteasistencia.component';
import { ListausuariosComponent } from './gestionUsuarios/listausuarios/listausuarios.component';
import { NuevousuarioComponent } from './gestionUsuarios/nuevousuario/nuevousuario.component';
import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';
import { NuevohorarioComponent } from './personalOperativo/nuevohorario/nuevohorario.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { DirectiveModule } from 'src/app/directive/directive.module';
import {ClipboardModule} from '@angular/cdk/clipboard';
// import { DashbardComponent } from './personalOperativo/dashbard/opciones.component';


@NgModule({
  declarations: [
    AdminComponent,
    DashbardComponent,
    NuevopersonalComponent,
    NuevocargoComponent,
    HorarioComponent,
    ReporteasistenciaComponent,
    ListausuariosComponent,
    NuevousuarioComponent,
    ConfiguracionesComponent,
    NuevohorarioComponent,
    Opciones
  
  
    

  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    DemoMaterialModule,
    ComponentsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    DirectiveModule,
    FormsModule,
    ClipboardModule
  ],
  exports: [
    AdminComponent
  ]
})
export class AdminModule { }
