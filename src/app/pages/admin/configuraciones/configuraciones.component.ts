import { Component, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { NuevohorarioComponent } from '../personalOperativo/nuevohorario/nuevohorario.component';
import { EmployeeService } from 'src/app/service/employee.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from 'src/app/service/validation.service';
import { FunctionService } from 'src/app/service/function.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Ihorarios } from 'src/app/interface/employee.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-configuraciones',
  templateUrl: './configuraciones.component.html',
  styleUrls: ['./configuraciones.component.css']
})
export class ConfiguracionesComponent implements AfterViewInit, OnDestroy {

  openTabUsers = 1;
  agregarNuevoCargo: boolean = false;
  agregarhorario: boolean = false;

  listRole:any;
  dataSourceRol:any = new MatTableDataSource([]);
  @ViewChild('pagCargo') paginatorRol!: MatPaginator;

  horarioColumns: string[] = [  'Horario',   'Acciones', ];
  datasourceTimeTable = new MatTableDataSource<Ihorarios>([]);
  @ViewChild('pagHorario') paginatorTimeTable!: MatPaginator;

  columnsRol:any=[];
  displayedColumnsRol!: string[];
  formRol!: FormGroup;
  data:any;
  copyed: boolean=false;

  private subTableTime!: Subscription

  constructor(
    public modal:MatDialog,
    private semployee:EmployeeService,
    private fb:FormBuilder,
    private sval:ValidationService,
    private sfn:FunctionService,
    private spinner:NgxSpinnerService
  )
  {
    this.ListRol();
    this.getTimeTable();
    this.CreateFormRol();
    this.data = this.sfn.LocalStorageSesionBusiness();
  }
  ngOnDestroy(): void {
    this.subTableTime.unsubscribe();
    console.log('me destrui');
  }

  ngAfterViewInit() {
    this.datasourceTimeTable.paginator = this.paginatorTimeTable;
  }

  ngOnInit(): void {
    this.subTableTime = this.semployee.newTimetable$.subscribe(res => {
      if (res) {
        this.getTimeTable();
      }
    })
  }

  tabsUsers($tabNumber: number) {
    this.openTabUsers = $tabNumber;
  }

  getTimeTable() {

    this.spinner.show();
    this.semployee.getTimeTable().subscribe( response =>{

      this.datasourceTimeTable = new MatTableDataSource<Ihorarios>(response);
      this.datasourceTimeTable.paginator = this.paginatorTimeTable;
      this.spinner.hide();
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  nuevoHorario(edit: boolean, item?: Ihorarios) {

    const body = {
      edit: edit,
      justWatch: false,
      close: false,
      ... item
    };

    this.modal.open(NuevohorarioComponent,{
      data: body,
      disableClose: true,
      panelClass: 'full-screen-modal',
    }).afterClosed().subscribe( res =>{
      if(res.exito) {
        this.getTimeTable();
      }
    });
  }

  async showdelete(data: any) {

    const headboardMessage = 'Estás Seguro de eliminar este horario';
    const message = 'Se eliminaran los datos guardados de este horario';
    const exito = await this.sfn.modalQuestion(headboardMessage, message);

    if (exito) {
      this.delete(data);
    }
  }

  delete(data: any) {

    this.spinner.show();
    const body = {
      idhorario: data.idhorario
    };

    this.semployee.deletetimetable(body).subscribe((response: any) => {

      const exito = String(response.message).toUpperCase() === 'HORARIO ELIMINADO';
      if (exito) this.getTimeTable();
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }


  /*ROLES*/
  CreateFormRol()
  {
    this.formRol = this.fb.group({
      idcargo: [0, Validators.required],
      cargo: ['', Validators.required],
      editar: [false, Validators.required]
    });
  }

  get Cargo(){
    return this.sval.ctrInvalid('cargo',this.formRol);
  }

  ListRol()
  {
    this.spinner.show();
    this.semployee.getRoles().subscribe( (response:any) => {
      this.listRole = response;
      this.dataSourceRol = new MatTableDataSource(response);
      const key = Object.keys(response[0]);
      let object:any = {columns:[], display:[]};
      key.forEach((e)=>{
        object.columns.push({title:e});
        object.display.push(e);
      });
      this.columnsRol = object.columns;
      this.displayedColumnsRol = object.display;
      this.dataSourceRol.paginator = this.paginatorRol;
      this.spinner.hide();
    },(e)=>{this.spinner.hide(); this.sfn.exception(e);});
  }

  ActionsRol(tipo:string,id:number)
  {
    const cargo = this.listRole.find((e:any)=> e.idcargo === id);
    if(tipo==='nuevo')
    {
      this.agregarNuevoCargo = true;
      this.CreateFormRol();
    }
    else if(tipo==='editar')
    {
      this.formRol.patchValue({
        idcargo: cargo.idcargo,
        cargo: cargo.cargo,
        editar: true
      });
      this.agregarNuevoCargo = true;
    }
    else
    {
      this.sfn.modalQuestion('Desea eliminar cargo','Desea eliminar cargo').then((e:any)=>{
        if(e.exito)
        {
          this.semployee.deleteRoles({idcargo:cargo.idcargo}).subscribe(response=> {
            console.log(response)
            this.ListRol();
          }, (e)=> this.sfn.exception(e));
        }
      });
    }
  }

  CrudRol()
  {
    if(this.formRol.invalid) return this.sval.emptyData(this.formRol);
    this.spinner.show();
    const cuerpo = {
      ...this.formRol.value
    }
    this.semployee.setRole(cuerpo).subscribe((response:any) =>{
      this.agregarNuevoCargo = false;
      this.ListRol();
      this.spinner.hide();
    }, (e)=>{this.spinner.hide(); this.sfn.exception(e)});
  }
  /*ROLES*/


  Copy()
  {
    this.sfn.openSnackBar('Codigo Copiado');
  }
}
