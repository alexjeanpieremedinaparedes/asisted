import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from 'src/app/service/employee.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidationService } from 'src/app/service/validation.service';
import { NuevopersonalComponent } from '../nuevopersonal/nuevopersonal.component';

@Component({
  selector: 'app-nuevocargo',
  templateUrl: './nuevocargo.component.html',
  styleUrls: ['./nuevocargo.component.css']
})
export class NuevocargoComponent implements OnInit {

  form!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private semployee: EmployeeService,
    private sval: ValidationService,
    private spinner: NgxSpinnerService,
    private sfn: FunctionService,
    private dialogRef: MatDialogRef<NuevopersonalComponent>
   ) {
     this.createForm();
   }

  ngOnInit(): void {
  }

  get cargoInvalid() {
    return this.sval.ctrInvalid('cargo', this.form);
  }

  createForm() {
    this.form = this.fb.group({
      idcargo: [ 0 ],
      cargo: [ '', [ Validators.required ] ],
      editar: [ false ],
    });
  }

  saveRole() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };

    this.semployee.setRole( body ).subscribe( (response: any) => {

      this.spinner.hide();
      const ok = response.status === 200;
      if(ok) {
        const message = response.message;
        this.sfn.openSnackBar(message);
        this.dialogRef.close( { exito: true } );
      }
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

}
