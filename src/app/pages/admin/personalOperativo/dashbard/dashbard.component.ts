import { MatDialog } from '@angular/material/dialog';
import { HorarioComponent } from '../horario/horario.component';
import { NuevopersonalComponent } from '../nuevopersonal/nuevopersonal.component';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeService } from 'src/app/service/employee.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Iemployee } from 'src/app/interface/employee.interface';
import { FunctionService } from 'src/app/service/function.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../../../../service/validation.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-dashbard',
  templateUrl: './dashbard.component.html',
  styleUrls: ['./dashbard.component.css'],
})
export class DashbardComponent implements AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  form!: FormGroup;

  openTab = 1;
  filtrar: boolean = false;

  listHorario: any[] = [];
  listCargo: any[] = [];
  listCargoCopy: any[] = [];
  reporteasistencia: boolean = false;
  displayedColumns: string[] = ['Nombres Completos', 'Cargo', 'Horario', 'Acciones',];
  dataSource = new MatTableDataSource<Iemployee>([]);
  listCopy: Iemployee[] = [];




  openBottomSheet(): void {
    this._bottomSheet.open(Opciones);
  }

  constructor(
    private modal: MatDialog,
    private semployee: EmployeeService,
    private spinner: NgxSpinnerService,
    private sfn: FunctionService,
    private fb: FormBuilder,
    private sval: ValidationService,
    private _bottomSheet: MatBottomSheet
  ) {
    this.getEmployee();
    this.createForm();
  }

  get cargoInvalid() {
    return this.sval.ctrInvalid('cargo', this.form);
  }

  get horarioInvalid() {
    return this.sval.ctrInvalid('horario', this.form);
  }

  createForm() {
    this.form = this.fb.group({
      cargo: [ '' ],
      horario: [ '', Validators.required ]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.semployee.newEmploye$.subscribe(res => {
      if (res) {
        this.getEmployee();
      }
    })
  }

  toggleTabs($tabNumber: number) {
    this.openTab = $tabNumber;
  }

  changeFill(check: boolean, item: any) {
    item.estado = check;
  }

  getEmployee() {

    this.listCargo = [];
    this.spinner.show();
    this.semployee.getEmployee().subscribe(response => {

      this.listCopy = response;
      this.dataSource = new MatTableDataSource<Iemployee>(response);
      this.dataSource.paginator = this.paginator;
      this.loadFilter();

      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  async showdelete(data: any) {

    const headboardMessage = 'Estás Seguro de eliminar a este personal operativo';
    const message = 'Se eliminaran los datos guardados de este usuario';
    const exito = await this.sfn.modalQuestion(headboardMessage, message);

    if (exito) {
      this.delete(data);
    }
  }

  delete(data: any) {

    this.spinner.show();
    const body = {
      idempleado: data.idempleado
    };

    this.semployee.deleteEmployee(body).subscribe((response: any) => {

      const exito = response.message === 'empleado eliminado';
      if (exito) this.getEmployee();
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  loadFilter() {
    const cargo = Object.entries(this.sfn.groupByKey(this.dataSource.data, 'cargo'));
    const horario = Object.entries(this.sfn.groupByKey(this.dataSource.data, 'horario'));

    for (let item of cargo) {
      const body = { cargo: item[0], estado: false };
      this.listCargo.push(body);
    }

    for (let item of horario) this.listHorario.push(item[0]);

    this.listCargoCopy = this.listCargo;
  }

  filter() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    const horario = this.form.value.horario;
    const cargo = this.listCargo.filter(x=>x.estado).map(x=>x.cargo);
    if( cargo.length > 0 ) {

      this.dataSource = new MatTableDataSource<Iemployee>(this.listCopy);
      this.dataSource = new MatTableDataSource<Iemployee>(this.dataSource.data.filter( result => {

        if( cargo.includes(result.cargo) && horario === result.horario ){
          return result;
        }
        return null;
      }));
    }
    else {
      this.dataSource = new MatTableDataSource<Iemployee>(this.listCopy);
    }
  }

  clearFilter() {

    this.form.reset();
    this.listCargo.forEach( x=>x.estado = false );
    this.listCargo = this.listCargoCopy;

    this.dataSource = new MatTableDataSource<Iemployee>(this.listCopy);
    this.dataSource.paginator = this.paginator;

  }

  searchFilter(text: string) {

    this.listCargo = this.listCargoCopy;
    if(text){
      this.listCargo = this.listCargo.filter(x=>String(x.cargo).toUpperCase().includes(text.toUpperCase()));
    }
  }

  modalPersonal(_new: boolean, item?: Iemployee) {

    const body = {
      new: _new,
      item: item
    };
    this.modal.open(NuevopersonalComponent, {
      data: body,
      disableClose: true,
      panelClass: 'full-screen-modal',

    }).afterClosed().subscribe(res => {
      if (res.exito) {
        this.getEmployee();
      }
    });
  }

  reporteAsistencia() {
    this.reporteasistencia = !this.reporteasistencia;
  }

  horario(idempleado:number) {
    this.modal.open(HorarioComponent,{data:idempleado}).afterClosed().subscribe(()=> {
      this.getEmployee();
    });
  }

}





@Component({
  selector: 'opciones',
  templateUrl: 'opciones.html',
})
export class Opciones {
  constructor(private _bottomSheetRef: MatBottomSheetRef<Opciones>,
              private modal: MatDialog,) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  modalPersonal(_new: boolean, item?: Iemployee) {

    const body = {
      new: _new,
      item: item
    };
    this.modal.open(NuevopersonalComponent, {
      data: body,
      disableClose: true
    }).afterClosed().subscribe(res => {
      if (res.exito) {

      }
    });
  }

}
