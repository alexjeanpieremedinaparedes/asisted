import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Ihorarios } from 'src/app/interface/employee.interface';
import { EmployeeService } from 'src/app/service/employee.service';
import { FunctionService } from 'src/app/service/function.service';
import { NuevohorarioComponent } from '../nuevohorario/nuevohorario.component';

@Component({
  selector: 'app-horario',
  templateUrl: './horario.component.html',
  styleUrls: ['./horario.component.css']
})
export class HorarioComponent implements OnInit {

  listHorario:any;
  idhorario!:number;

  constructor(
    public modals:MatDialog,
    private employee:EmployeeService,
    private fn:FunctionService,
    @Inject(MAT_DIALOG_DATA) public data:any
  )
  {
    this.ListTime();
  }

  ngOnInit(): void {
  }

  nuevoHorario() {

    const body = {
      edit: false,
      justWatch: false,
      close: true
    };

    this.modals.open(NuevohorarioComponent,{
      data: body,
      disableClose: true,
      panelClass: 'full-screen-modal',
    }).afterClosed().subscribe( res =>{
      if(res.exito) {
        this.ListTime();
      }
    });
  }

  ListTime()
  {
    this.employee.getTimeTable().subscribe(response=> {
      this.listHorario = response;
    });
  }

  getvalue(e:any,value:any)
  {
    this.idhorario = value.idhorario;
  }

  setTimeEmployee()
  {
    console.log(this.data);
    this.employee.setTimeEmpleyee({idempleado:this.data,idhorario:this.idhorario}).subscribe(response=> {
      this.fn.openSnackBar("Horario asignado");
      this.modals.closeAll();
    },e => { this.fn.exception(e); });
  }

}
