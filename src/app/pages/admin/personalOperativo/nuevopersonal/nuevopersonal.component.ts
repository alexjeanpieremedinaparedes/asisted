import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Iroles, Ihorarios, ItypeDocuento, Iemployee } from 'src/app/interface/employee.interface';
import { EmployeeService } from 'src/app/service/employee.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidationService } from 'src/app/service/validation.service';
import { DashbardComponent } from '../dashbard/dashbard.component';
import { NuevocargoComponent } from '../nuevocargo/nuevocargo.component';
import { NuevohorarioComponent } from '../nuevohorario/nuevohorario.component';
import { OthersService } from 'src/app/service/others.service';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-nuevopersonal',
  templateUrl: './nuevopersonal.component.html',
  styleUrls: ['./nuevopersonal.component.css']
})
export class NuevopersonalComponent implements OnInit, OnDestroy {

  bs64Photo: string = 'NO';
  listRoles: Iroles[] = [];
  listTimeTable: Ihorarios[] = [];
  listTypeDocument: ItypeDocuento[] = [];
  form!: FormGroup;
  
  private subTableTime!: Subscription

  constructor(
    private modal: MatDialog,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private semployee: EmployeeService,
    private sothers: OthersService,
    private sval: ValidationService,
    private sfn: FunctionService,
    private dialogRef: MatDialogRef<DashbardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.getRoles();
    this.getTimeTable();
    this.getTypeDocument();

    this.createForm();
    if(!this.data.new) this.showEditEmployee();
  }
  ngOnDestroy(): void {
    this.subTableTime.unsubscribe();
    console.log('me destrui');
  }

  ngOnInit(): void {
    this.subTableTime = this.semployee.newTimetable$.subscribe(res => {
      if (res) {
        this.getTimeTable();
      }
    })
  }

  get typeDocumentInvalid() {
    return this.sval.ctrInvalid('idtipodocumento', this.form);
  }

  get nDocumentoInvalid() {
    return this.sval.ctrInvalid('numerodocumento', this.form);
  }

  get apaternoInvalid() {
    return this.sval.ctrInvalid('apaterno', this.form);
  }

  get amaternoInvalid() {
    return this.sval.ctrInvalid('amaterno', this.form);
  }

  get nombresInvalid() {
    return this.sval.ctrInvalid('nombres', this.form);
  }

  get fechaNaciInvalid() {
    return this.sval.ctrInvalid('fechanacimiento', this.form);
  }

  get codigoubigeoInvalid() {
    return this.sval.ctrInvalid('codigoubigeo', this.form);
  }

  get fechainscripcionInvalid() {
    return this.sval.ctrInvalid('fechainscripcion', this.form);
  }

  get fechaemisiondniInvalid() {
    return this.sval.ctrInvalid('fechaemisiondni', this.form);
  }

  get fechacaducidadInvalid() {
    return this.sval.ctrInvalid('fechacaducidad', this.form);
  }
  
  get grupovotacionInvalid() {
    return this.sval.ctrInvalid('grupovotacion', this.form);
  }

  get cargoInvalid() {
    return this.sval.ctrInvalid('idcargo', this.form);
  }

  createForm() {
    this.form = this.fb.group({
      idempleado:       [ 0 ],
      idtipodocumento:  [ '', [ Validators.required ] ],
      idcargo:          [ '' , [ Validators.required ] ],
      idhorario:        [ 0 ],
      numerodocumento:  [ '', [ Validators.required ] ],
      apaterno:         [ '', [ Validators.required ] ],
      amaterno:         [ '', [ Validators.required ] ],
      nombres:          [ '', [ Validators.required ] ],
      fechanacimiento:  [ '', [ Validators.required ] ],
      fechainscripcion: [ '', [ Validators.required ] ],
      fechaemisiondni:  [ '', [ Validators.required ] ],
      fechacaducidad:   [ '', [ Validators.required ] ],
      codigoubigeo:     [ '', [ Validators.required ] ],
      grupovotacion:    [ '', [ Validators.required ] ],
      editar:           [ false ],
      imgbase64:        [ '' ],
      urlimage:         [ '' ]
    });
  }

  dateMoment( dato: any) {
    const date = new Date(dato +"T00:00:00");
    return date;
  }

  showEditEmployee() {

    const item: Iemployee = this.data.item;
    this.form.patchValue({
      idempleado:       item.idempleado,
      idtipodocumento:  item.idtipodocumento,
      idcargo:          item.idcargo,
      idhorario:        item.idhorario,
      numerodocumento:  item.numerodocumento,
      apaterno:         item.apellidoparteno,
      amaterno:         item.apellidomaterno,
      nombres:          item.nombre,
      fechanacimiento:  this.dateMoment(item.fechanacimiento),
      fechainscripcion: this.dateMoment(item.fechainscripcion),
      fechaemisiondni:  this.dateMoment(item.fechaemision),
      fechacaducidad:   this.dateMoment(item.fechacaducudad),
      codigoubigeo:     item.codigoubigeo,
      grupovotacion:    item.grupovotacion,
      editar:           true,
      imgbase64:        '',
      urlimage:         item.urlfoto
    });

    this.bs64Photo = item.urlfoto;
  }

  getReniec() {

    if(this.form.get('numerodocumento')?.invalid) return;

    this.spinner.show();
    const documento = this.form.value.numerodocumento ?? '';
    if(!documento) return;
    this.sothers.getReniec(documento).subscribe( response => {

      const ok = response.message === 'found data';
      if(ok) {
        
        const result = response.result;
        const ubigeo = result.ubicacion ?? '';

        const fnaci = moment(moment(result.feNacimiento, 'DD-MM-YYYY')).format('YYYY-MM-DD');
        const fins = moment(moment(result.feInscripcion, 'DD-MM-YYYY')).format('YYYY-MM-DD');
        const femi = moment(moment(result.feEmision, 'DD-MM-YYYY')).format('YYYY-MM-DD');
        const fcad = moment(moment(result.feCaducidad, 'DD-MM-YYYY')).format('YYYY-MM-DD');

        this.form.patchValue({
          numerodocumento:  result.nuDni,
          apaterno:         result.apePaterno,
          amaterno:         result.apeMaterno,
          nombres:          result.preNombres,
          fechanacimiento:  this.dateMoment( fnaci ),
          fechainscripcion: this.dateMoment( fins ),
          fechaemisiondni:  this.dateMoment( femi ),
          fechacaducidad:   this.dateMoment( fcad ),
          codigoubigeo: ubigeo.ubigeoreniec ?? '',
          grupovotacion: ''
        });
      }
      
      this.spinner.hide();
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  getRoles() {

    this.spinner.show();
    this.semployee.getRoles().subscribe( response =>{

      this.listRoles = response;
      this.spinner.hide();
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  getTimeTable() {

    this.spinner.show();
    this.semployee.getTimeTable().subscribe( response =>{

      this.listTimeTable = response;
      this.spinner.hide();
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  getTypeDocument() {

    this.spinner.show();
    this.semployee.getTypeDocument().subscribe( response =>{
      
      this.listTypeDocument = response;
      if(response.length > 0){
        const id = response[0].idtipodocumento;
        this.form.patchValue({ idtipodocumento : id });
      }
      this.spinner.hide();
    }, (e)=> {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }
  
  saveEmployee() {

    if(this.form.invalid) {
      return this.sval.emptyData(this.form);
    }

    this.spinner.show();
    const body = { ... this.form.value };
    body.numerodocumento = String(body.numerodocumento);

    this.semployee.setEmployee(body).subscribe( (response: any) => {

      this.spinner.hide();
      const ok = response.status === 200;
      if(ok) {
        const message = response.message;
        this.sfn.openSnackBar(message);
        this.clear();
        if(!this.data.new) this.dialogRef.close( { exito: true } );
        else this.semployee.newEmploye$.emit(this.data.new);
      }
    }, (e) => {
      this.spinner.hide();
      this.sfn.exception(e);
    });
  }

  clear() {
    this.form.reset();
    this.form.patchValue({
      idempleado:       0,
      idcargo:          0,
      idhorario:        0,
      editar:           false
    });

    this.bs64Photo = 'NO';

    if( this.listTypeDocument.length > 0 ){
      this.form.patchValue({ idtipodocumento : this.listTypeDocument[0].idtipodocumento });
    }
  }

  uploadFileEvt(imgFile: any) {

    this.bs64Photo = '';
    if (imgFile.target.files && imgFile.target.files[0]) {

      let reader = new FileReader();

      reader.onload = (e: any) => {

        let image = new Image();
        image.src = e.target.result;
        image.onload = rs => {

          let imgBase64Path = e.target.result;
          const bs64split = imgBase64Path.split(',')[1];
          this.bs64Photo = this.sfn.convertbs64ForSrc(bs64split);
          this.form.patchValue({ imgbase64: bs64split });
        };
      };

      reader.readAsDataURL(imgFile.target.files[0]);
      // if(this.fileInput)this.fileInput.nativeElement.value = "";
    }
  }
  
  nuevocargo() {

    this.modal.open(NuevocargoComponent, { disableClose: true,  }).afterClosed().subscribe( res => {
      if(res.exito){
        this.getRoles();
      }
    });
  }

  nuevoHorario() {
    const body = {
      edit: false,
      justWatch: false,
      close: false
    };

    this.modal.open(NuevohorarioComponent,{ 
      data: body,
      disableClose: true,
      panelClass: 'full-screen-modal', 
     }).afterClosed();
  }

  visualizeTimeTable() {

    const idtimeTable = this.form.value.idhorario;
    const item = this.listTimeTable.find(x=>x.idhorario === idtimeTable);
    if(!item) return;
    const body = {
      justWatch: true,
      ... item
    };

    this.modal.open(NuevohorarioComponent,{
      data: body,
      disableClose: true
    }).afterClosed().subscribe( res =>{
      if(res.exito) {
        this.getTimeTable();
      }
    });
  }

  public onlyNumberAllowed( $event: any ): boolean {

    const id = Number(this.form.value.idtipodocumento);
    const document = this.listTypeDocument.find(x=>x.idtipodocumento === id);
    let len = 0;
    if(document) {
      const typeDoc = document?.tipodocumento;
      switch(typeDoc) {
        case 'DNI':
          this.form.get('numerodocumento')?.setValidators([ Validators.minLength(8), Validators.maxLength(8)]);
          this.form.get('numerodocumento')?.markAsTouched();

          len = 8;
          break;
      }
      this.form.get('numerodocumento')?.updateValueAndValidity();
    }
    return this.sfn.onlyNumberAllowed($event, len);
  }

}
