import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { IrolesDetalle1 } from 'src/app/interface/employee.interface';
import { EnvSystem } from 'src/app/model/env.model';
import { EmployeeService } from 'src/app/service/employee.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidationService } from 'src/app/service/validation.service';
import { NuevopersonalComponent } from '../nuevopersonal/nuevopersonal.component';

@Component({
  selector: 'app-nuevohorario',
  templateUrl: './nuevohorario.component.html',
  styleUrls: ['./nuevohorario.component.css']
})
export class NuevohorarioComponent implements OnInit {

  public env = new EnvSystem();
  openTabUsers: string = 'MAÑANA';
  form!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private semployee: EmployeeService,
    private spinner: NgxSpinnerService,
    private sfn: FunctionService,
    private sval: ValidationService,
    private dialogRef: MatDialogRef<NuevopersonalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.createForm();
    this.addHorario();
    if (data.edit || data.justWatch) this.loadEditTimeTable();
  }

  ngOnInit(): void { }

  tabsUsers(turno: string) {
    this.openTabUsers = turno;
  }

  get denominacionInvalid() {
    return this.sval.ctrInvalid('denominacion', this.form);
  }

  get detallehorario() {
    return this.form.get('dethorario') as FormArray;
  }

  createForm() {

    this.form = this.fb.group({
      idhorario: [0],
      denominacion: ['', [Validators.required]],
      editar: [false],
      dethorario: this.fb.array([]),
    });
  }

  loadEditTimeTable() {

    this.form.patchValue({
      idhorario: this.data.idhorario,
      denominacion: this.data.horario,
      editar: true
    });

    const detalle: IrolesDetalle1[] = this.data.detalle;
    detalle.forEach((h) => {

      if (h.detalle) {

        h.detalle.forEach((det, ihora) => {

          this.detallehorario.value.forEach((el: any, index: number) => {

            const diaOK = el.dia.toUpperCase() === h.dia.toUpperCase();
            const turnoOK = el.turno.toUpperCase() === det.turno.toUpperCase();
            if (diaOK && turnoOK) {

              this.detallehorario.at(index).get('status')?.setValue(true);
              this.detallehorario.at(index).get('id')?.setValue(h.idhorario);
              this.detallehorario.at(index).get('idhorariodetalle')?.setValue(h.idhorariodetalle);

              const len = this.detallehora(index).length;
              const ok = this.detallehora(index).at(len - 1).get('horainicio')?.value;
              const indexHorario = len - 1;
              const add = !ok && len <= 1 ? false : true;

              if (!add) {
                this.detallehora(index).at(indexHorario).get('horainicio')?.setValue(det.horainicio);
                this.detallehora(index).at(indexHorario).get('horasalida')?.setValue(det.horasalida);

                this.detallehora(index).at(indexHorario).get('id')?.setValue(det.idhorariodetalle);
                this.detallehora(index).at(indexHorario).get('idhorariodetallehora')?.setValue(det.idhorariodetallehora);
              }
              else {
                this.detallehora(index).push(this.newTime(det.turno, el.dia, det.idhorariodetalle, det.idhorariodetallehora, det.horainicio, det.horasalida));
              }
            }
          });
        });
      }
    });
  }

  detallehora(index: number): FormArray {
    return this.detallehorario.at(index).get("dethora") as FormArray
  }

  valueHorario(index: number, value: string) {
    return this.detallehorario.controls[index].get(value)?.value;
  }

  newHorario(day: string, turno: string): FormGroup {

    return this.fb.group({
      id: [0],
      idhorariodetalle: [0],
      dia: [day],
      status: [false],
      turno: [turno],
      dethora: this.fb.array([])
    });
  }

  newTime(turno: string, day: string, id: number, idHoradetalle: number, horaI: string, horaF: string): FormGroup {
    return this.fb.group({
      id: [id],
      idhorariodetallehora: [idHoradetalle],
      dia: [day],
      turno: [turno],
      horainicio: [horaI],
      horasalida: [horaF]
    });
  }

  addHorario() {

    const turno = ['MAÑANA', 'TARDE', 'NOCHE'];
    const days = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];

    for (let item of turno) {

      days.forEach((el) => {

        const i = this.detallehorario.length;

        this.detallehorario.push(this.newHorario(el, item));
        this.detallehora(i).push(this.newTime(item, el, 0, 0, '', ''));
      });
    }
  }

  addTime(index: number) {

    const cant = this.detallehora(index).length;
    const turno = this.detallehorario.at(index).get('turno')?.value;
    const day = this.detallehorario.at(index).get('dia')?.value;

    if (cant < this.env.capacityMaxhorario) {
      this.detallehora(index).push(this.newTime(turno, day, 0, 0, '', ''));
      const len = this.detallehora(index).length;
      for (let i = 0; i < len; i++) {
        this.detallehora(index).at(i).get('horainicio')?.setValidators(Validators.required);
        this.detallehora(index).at(i).get('horasalida')?.setValidators(Validators.required);

        this.detallehora(index).at(i).get('horainicio')?.updateValueAndValidity();
        this.detallehora(index).at(i).get('horasalida')?.updateValueAndValidity();
      }
    }
  }

  removeTime(index: any) {

    let cant = this.detallehora(index).length;
    if (cant > 1) {
      this.detallehora(index).removeAt(cant - 1);
    }
  }

  changeDay(check: boolean, index: number) {

    this.detallehorario.at(index).get('status')?.setValue(check);
    const cant = this.detallehora(index).length;

    for (let i = 0; i < cant; i++) {

      if (check) {
        this.detallehora(index).at(i).get('horainicio')?.setValidators(Validators.required);
        this.detallehora(index).at(i).get('horasalida')?.setValidators(Validators.required);
      }
      else {
        this.detallehora(index).at(i).get('horainicio')?.clearValidators();
        this.detallehora(index).at(i).get('horasalida')?.clearValidators();

        this.detallehora(index).at(i).get('horainicio')?.setValue(null);
        this.detallehora(index).at(i).get('horasalida')?.setValue(null);
      }
      this.detallehora(index).at(i).get('horainicio')?.updateValueAndValidity();
      this.detallehora(index).at(i).get('horasalida')?.updateValueAndValidity();
    }
  }

  public saveTimeTable() {

    if (this.form.invalid) {

      this.form.controls.dethorario.markAllAsTouched();
      return this.sval.emptyData(this.form);
    }

    let listDetalle: any[] = [];
    let detallehorario: any[] = [];
    const det = this.detallehorario.value.filter((x: any) => x.status);
    const detGroup = Object.entries(this.sfn.groupByKey(det, 'dia'));
    detGroup.forEach((el: any, i) => {

      const filterIdDetalle = el[1].filter((x: any) => x.idhorariodetalle > 0);

      const id = filterIdDetalle.length > 0 ? filterIdDetalle[0].id : i ?? i;
      const idhorariodetalle = filterIdDetalle.length > 0 ? filterIdDetalle[0].idhorariodetalle : i ?? i;

      // const id = el[1][0].id ?? i;
      // const idhorariodetalle = el[1][0].idhorariodetalle ?? i;

      const body = {
        id: id,
        idhorariodetalle: idhorariodetalle,
        dia: el[0]
      };

      detallehorario.push(body);
    })

    const listHora = this.detallehorario.value.filter((x: any) => x.status).map((x: any) => x.dethora);
    listHora.forEach((el: any) => { for (let i = 0; i < el.length; i++) listDetalle.push(el[i]); });
    if(!this.validateTableTime(listDetalle.length)) return;
    
    this.spinner.show();
    const body = {
      ... this.form.value,
      detallehorario: detallehorario,
      detallehora: listDetalle
    };

    delete body.dethorario;
    this.semployee.setTimeTable(body).subscribe((response: any) => {

      this.spinner.hide();
      const ok = response.status === 200;
      if (ok) {
        const message = response.message;
        this.sfn.openSnackBar(message);

        this.clearForm();
        if (this.data.edit) this.dialogRef.close({ exito: true });
        else this.semployee.newTimetable$.emit(true);

        if (this.data.close) this.dialogRef.close({ exito: true });
      }
    }, (e) => {
      this.spinner.hide();
      this.sfn.exception(e);
    })
  }

  private validateTableTime(cant: number): boolean {

    if( cant <= 0 ) {
      const message = 'Debe ingresar al menos un horario';
      const e = { error: { message: message } };
      this.sfn.exception(e);
      return false;
    }
    return true;
  }

  clearForm() {

    this.form.reset();
    this.form.patchValue({
      idhorario: 0,
      editar: false
    });

    this.detallehorario.clear();
    this.addHorario();
    this.openTabUsers = 'MAÑANA';
  }

  private time(value: string, iDay: number, itime: number, invalid: boolean) {

    if(invalid) {
      this.detallehora(iDay).at(itime).get(value)?.setErrors({ 'incorrect': invalid });
      this.detallehora(iDay).at(itime).get(value)?.markAsTouched();
    }
    else {
      this.detallehora(iDay).at(itime).get(value)?.setErrors({'firstError': null});
      this.detallehora(iDay).at(itime).get(value)?.updateValueAndValidity();
    }
  }

  private returnValueTime(value: string, iDay: number, itime: number): any{
    return this.detallehora(iDay).at(itime).get(value)?.value;
  }

  public timeInvalid(iDay: number) {

    this.detallehorario.at(iDay)
      .get('dethora')?.value
      .forEach((el: any, i: number) => {

        // TODO: HORA ENTRADA
        const entryValue = this.sfn.parseTime(el.horainicio);
        const entryTime = this.sfn.parseTimeInt(entryValue);
        // TODO: HORA SALIDA
        const departureValue = this.sfn.parseTime(el.horasalida);
        const departureTime = this.sfn.parseTimeInt(departureValue);

        // TODO: VALIDAR HORA ENTRADA
        if(entryTime >= departureTime && el.turno !== 'NOCHE' ) this.time('horainicio', iDay, i, true);
        else this.time('horainicio', iDay, i, false);

        // TODO: VALIDAR HORA SALIDA
        if(departureTime <= entryTime && el.turno !== 'NOCHE' ) this.time('horasalida', iDay, i, true);
        else this.time('horasalida', iDay, i, false);

        // TODO: VALIDAR EL SIGUIENTE HORARIO NO DEBE SER MENOR AL ULTIMO HORARIO DE SALIDA
        if( i > 0 ) {

          const indexlastExit = i - 1;
          const lastExitValue = this.sfn.parseTime(this.returnValueTime('horasalida', iDay, indexlastExit));
          const lastExitTime = this.sfn.parseTimeInt(lastExitValue);

          if( entryTime < lastExitTime ) {
            this.time('horainicio', iDay, i, true)
          }
        }

        this.validateForTurn(el.turno, iDay, i, entryTime);
      });
  }

  validateForTurn( turn: string, iDay: number, itime: number, entryTime: number ) {

    switch( turn ) {
      case 'MAÑANA':
        if( entryTime >= 120000 ) {
          this.time('horainicio', iDay, itime, true );
        }
        break;

      default:
        if( entryTime < 120000 ) {
          this.time('horainicio', iDay, itime, true );
        }
        break;
    }
  }

}
