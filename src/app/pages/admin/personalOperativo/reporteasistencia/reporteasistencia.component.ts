
import { AfterViewInit, Component, ViewChild, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { ReportService } from 'src/app/service/report.service';
import { ValidationService } from 'src/app/service/validation.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionService } from 'src/app/service/function.service';

@Component({
  selector: 'app-reporteasistencia',
  templateUrl: './reporteasistencia.component.html',
  styleUrls: ['./reporteasistencia.component.css']
})
export class ReporteasistenciaComponent implements OnInit {

  @Input() reporteasistencia: boolean = false;
  @Output() Send  = new EventEmitter<boolean>();
  notreports: boolean = false;

  form!:FormGroup;
  listEmployee:any = [];
  listreport:any = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  dataSource = new MatTableDataSource([]);
  displayedColumns!: string[];
  columsn:any = [];
  verp:boolean= false;
  totalhorastrabajadas!:string;
  horario!:string;

  constructor(
    private report:ReportService,
    private fb:FormBuilder,
    private sval:ValidationService,
    private employee:EmployeeService,
    private spinner:NgxSpinnerService,
    private fn:FunctionService
  )
  {
    this.createForm();
    this.ListEmployee();
  }

  ngOnInit(): void {
  }

  reporteAsistencias(){
    this.reporteasistencia = !this.reporteasistencia;
    this.Send_emiter();
  }

  Send_emiter(){
    this.Send.emit( this.reporteasistencia);
  }

  createForm()
  {
    this.form = this.fb.group({
      fechainicio: ['', Validators.required],
      fechafin: ['', Validators.required],
      numerodocumento: [ ''],
      idempleado: ['', Validators.required]
    });
  }

  get IDEmployee(){
    return this.sval.ctrInvalid('idempleado',this.form);
  }
  get StartDate(){
    return this.sval.ctrInvalid('fechainicio',this.form);
  }
  get EndDate(){
    return this.sval.ctrInvalid('fechafin',this.form);
  }

  ListEmployee()
  {
    this.report.getEmployee().subscribe(response=> {
      this.listEmployee = response;
    });
  }

  Selectemployee(em:any)
  {
    const nemployee = this.listEmployee.find( (e:any) => e.idempleado == em);
    this.form.patchValue({
      numerodocumento: nemployee.numerodocumento
    });
  }

  ListReport()
  {
    this.verp = true;
    if(this.form.invalid) return this.sval.emptyData(this.form);
    this.spinner.show();
    const body = { ...this.form.value };
    this.listreport = [];
    this.report.AttendanceReportList(body).subscribe(response => {

      this.listreport = response.result;
      this.verp = this.listreport.length>10 ? true : false;
      if(this.listreport.length>0)
      {
        this.totalhorastrabajadas = this.listreport[0].totalhoras;
        this.horario = this.listreport[0].horario;
        this.dataSource = new MatTableDataSource(this.listreport);
        const key = Object.keys(this.listreport[0]);
        let object:any = { columns: [] , display:[] };

        key.forEach((e,i)=> {
          object.columns.push({ title:e });
          object.display.push(e);
        });

        this.columsn = object.columns;
        this.displayedColumns = object.display;
        this.dataSource.paginator = this.paginator;
      }
      this.spinner.hide();
    }, (e) => {
      this.spinner.hide();
      this.fn.exception(e)
    });
  }

  public exportToExel() {

    if(this.dataSource.data.length>0){
      const name = 'Reporte de Asistencia'
      this.fn.exportToExcel(this.dataSource.data, name);
    }
    else {
      const e = {error: {message: 'No hay datos para exportar'}};
      this.fn.exception(e);
    }
  }
}
