import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { IListUser } from 'src/app/interface/user.interface';
import { FunctionService } from 'src/app/service/function.service';
import { UserService } from 'src/app/service/user.service';
import { NuevousuarioComponent } from '../nuevousuario/nuevousuario.component';

@Component({
  selector: 'app-listausuarios',
  templateUrl: './listausuarios.component.html',
  styleUrls: ['./listausuarios.component.css']
})
export class ListausuariosComponent implements AfterViewInit {

  displayedColumns!: string[];
  columns:any;
  dataSource = new MatTableDataSource([]);
  ListUser:any;

  constructor(
    public modal:MatDialog,
    private user:UserService,
    private fn:FunctionService,
    private spinner:NgxSpinnerService
  )
  {
    this.getUser();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
  }

  nuevousuario(id:number,eliminar:boolean)
  {
    if(!eliminar)
    {
      const user = this.ListUser.find((e:any)=> e.idusuario === id);
      this.modal.open(NuevousuarioComponent,{data:user, panelClass: 'full-screen-modal',}).afterClosed().subscribe(()=>{
        this.getUser();
      });
    }
    else
    {
      this.fn.modalQuestion('Desea eliminar usuario','Desea eliminar usuario').then((e:any)=> {
        this.spinner.show();
        if(e.exito)
        {
          this.user.deleteUser({idusuario:id}).subscribe(response=> {
            this.getUser();
            this.spinner.hide();
          },(e)=> {this.spinner.hide(); this.fn.exception(e);})
        }
      });
    }
  }

  getUser()
  {
    this.spinner.show();
    this.user.getUser().subscribe((response:any)=> {
      this.ListUser = response.result;

      console.log(this.ListUser)
      this.dataSource = new MatTableDataSource(this.ListUser);
      this.dataSource.paginator = this.paginator;
      const key = Object.keys(this.ListUser[0]);
      let object:any = {columns:[],display:[]};
      key.forEach((e)=> {
        object.columns.push({title:e});
        object.display.push(e);
      });
      this.columns = object.columns;
      this.displayedColumns = object.display;
      this.spinner.hide();
    },(e)=> {this.spinner.hide(); this.fn.exception(e)});
  }
}
