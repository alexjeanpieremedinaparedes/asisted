import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from 'src/app/service/employee.service';
import { FunctionService } from 'src/app/service/function.service';
import { UserService } from 'src/app/service/user.service';
import { ValidationService } from 'src/app/service/validation.service';

@Component({
  selector: 'app-nuevousuario',
  templateUrl: './nuevousuario.component.html',
  styleUrls: ['./nuevousuario.component.css']
})
export class NuevousuarioComponent implements OnInit {

  listEmployee:any;
  listForm:any;
  openTabUsers = 1;
  form!:FormGroup;
  constructor(
    private fb:FormBuilder,
    private employee:EmployeeService,
    private fn:FunctionService,
    private user:UserService,
    private sval:ValidationService,
    private modal:MatDialog,
    private spinner:NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public data:any
  )
  {
    this.CreateForm();
    this.ListEmployee();
    this.ListForm();

    if(data!= undefined)
    {
      this.form.patchValue({
        idusuario: this.data.idusuario,
        idempleado: this.data.idempleado,
        usuario: this.data.usuario,
        password: this.data.password,
        editar:true,
        permisos:[]
      });
    }
  }

  ngOnInit(): void {
  }

  tabsUsers($tabNumber: number) {
    this.openTabUsers = $tabNumber;
  }


  CreateForm()
  {
    this.form = this.fb.group({
      idusuario:[0, Validators.required],
      idempleado:[0, Validators.required],
      usuario:[ '', Validators.required],
      password:[ '', Validators.required ],
      editar:[ false, Validators.required],
      permisos:[[]]
    });
  }

  get Usuario() { return this.sval.ctrInvalid('usuario',this.form); }
  get Password() { return this.sval.ctrInvalid('password',this.form); }


  ListEmployee()
  {
    this.employee.getEmployee().subscribe(response=> {
      this.listEmployee = response;
    },(e)=>this.fn.exception(e));
  }

  ListForm()
  {
    this.user.getForm().subscribe(response=> {
      this.listForm = response.result;

      if(this.data != undefined)
      {
        const l = this.data.permisos;
        if(l.length>0)
        {
          for (let i = 0; i < this.listForm.length; i++)
          {
            this.listForm[i].estado = l.find((e:any)=> e.idformulario === this.listForm[i].idformulario).estado;
          }
        }
      }
    },(e)=>this.fn.exception(e));
  }

  setUser()
  {
    if(this.form.invalid) return this.sval.emptyData(this.form);
    this.spinner.show();
    const body = {
      ...this.form.value
    }
    body.permisos = this.listForm;
    console.log(body);
    this.user.setUser(body).subscribe(response=> {
      this.spinner.show();
      this.modal.closeAll();
      const status = this.form.get('editar')?.value ? 'edito' : 'agrego';
      const message = `El usuario se ${status} con exito`;
      this.fn.openSnackBar(message);
    },(e)=> {this.spinner.show(); this.fn.exception(e)});
  }
}
