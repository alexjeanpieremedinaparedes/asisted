import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { IdentidComponent } from 'src/app/auth/modales/identid/identid.component';
import { AsistenciaguardadaComponent } from 'src/app/components/asistenciaguardada/asistenciaguardada.component';
import { AttendanceService } from 'src/app/service/attendance.service';
import { FunctionService } from 'src/app/service/function.service';
import { ValidationService } from 'src/app/service/validation.service';

@Component({
  selector: 'app-controlasistencia',
  templateUrl: './controlasistencia.component.html',
  styleUrls: ['./controlasistencia.component.css']
})
export class ControlasistenciaComponent implements OnInit {

  form!:FormGroup;
  horaActual!:string;
  hora!:number;
  minuto!:string;
  tipo!:boolean;

  constructor(
    public modals: MatDialog,
    private fb:FormBuilder,
    private attendance:AttendanceService,
    private sval:ValidationService,
    private fn: FunctionService,
    private spinner: NgxSpinnerService,
    private close: MatDialogRef<null>,
    @Inject (MAT_DIALOG_DATA) public data:any
  )
  {
    this.createForm();
    const date = new Date();
    const hora = date.getHours();
    const minuto = date.getMinutes();
    this.tipo = data;
    this.horaActual = `${hora}:${minuto}`;
  }

  ngOnInit(): void {
    setInterval(() => {
      const horaactual = new Date();
      this.hora = horaactual.getHours();
      this.minuto = horaactual.getMinutes()< 10 ? `0${horaactual.getMinutes()}` : `${horaactual.getMinutes()}`;
    }, 1000);
  }

  identificacion(){
    this.modals.open(IdentidComponent, {
      panelClass: 'full-screen-modal',
    });
  }

  asistenciaguardad(){
    this.modals.open(AsistenciaguardadaComponent);
  }

  createForm()
  {
    this.form = this.fb.group({
      numerodocumento: ['', Validators.required],
      codigoubigeo   : [''],
      grupovotacion  : [''],
      tipo           : [ this.data ]
    });
  }

  get numeroDocumento() { return this.sval.ctrInvalid('numerodocumento',this.form); }
  get codigoUbigeo() { return this.sval.ctrInvalid('codigoubigeo',this.form); }
  get grupoVotacion() { return this.sval.ctrInvalid('grupovotacion',this.form); }

  Attendance()
  {
    if(this.form.invalid) return this.sval.emptyData(this.form);
    this.spinner.show();
    const body = { ...this.form.value };
    this.attendance.Assistance(body).subscribe(response =>{
      this.fn.openSnackBar('Asistencia marcada');
      this.spinner.hide();
      this.close.close(body);
    },(e)=> { this.spinner.hide(); this.fn.exception(e)});
  }

  public closeForm() {
    const body = { ...this.form.value };
   this.close.close(body); 
  }
}
