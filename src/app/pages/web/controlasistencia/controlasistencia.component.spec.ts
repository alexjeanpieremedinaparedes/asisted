import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlasistenciaComponent } from './controlasistencia.component';

describe('ControlasistenciaComponent', () => {
  let component: ControlasistenciaComponent;
  let fixture: ComponentFixture<ControlasistenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlasistenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlasistenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
