import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WebComponent } from './web.component';
import { InicioComponent } from './inicio/inicio.component';


const routes: Routes = [
    { path: 'web', component: WebComponent ,

    children:[
        { path: 'inicio', component: InicioComponent},
        { path: '**', redirectTo:'inicio', pathMatch: 'full'},
      ]
},
   

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class webRoutingModule {}
