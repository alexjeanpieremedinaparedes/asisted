import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AttendanceService } from 'src/app/service/attendance.service';
import { FunctionService } from 'src/app/service/function.service';
import { ControlasistenciaComponent } from '../controlasistencia/controlasistencia.component';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  AttendanceList:any = [];
  FechaActual!:string;
  now!:Date;
  hora!:number;
  minuto!:string;
  ip!:string;
  zonahoraria!:string;
  tiempodia!:string;

  constructor(
    public modal:MatDialog,
    private fn: FunctionService,
    private attendance: AttendanceService,
    private spinner: NgxSpinnerService,
    private router: Router
  )
  {
    if(this.fn.LocalStorageSesionUser()) this.router.navigateByUrl('/admin/dashboard', { replaceUrl: true });
    this.ListDailyAssistance(false);

    const date = new Date();
    this.FechaActual = date.toLocaleDateString().replace('/','-').replace('/','-').replace('/','-');
    this.tiempodia = date.getHours()<12 ? 'Buenos Dias' : date.getHours()<18 ? 'Buenas Tardes' : 'Buenas Noches';

    this.fn.getIP().subscribe((res:any)=> {
      this.ip = res.ip
    });

    this.fn.getLocation().subscribe((res:any)=> {
      this.zonahoraria = res.timezone
    });
  }

  ngOnInit(): void {
    this.now = new Date();
    setInterval(() => {
      this.now = new Date();
      this.hora = this.now.getHours();
      this.minuto = this.now.getMinutes()< 10 ? `0${this.now.getMinutes()}` : `${this.now.getMinutes()}`;
    }, 1000);
  }


  // Modal de Indetificacion
  controlAsistencia(tipo:boolean){
    this.modal.open(ControlasistenciaComponent,
      {
        data:tipo,
        panelClass: 'full-screen-modal'
      }).afterClosed().subscribe( result => {

        if(!result) return;
        const document = result.numerodocumento;
        if(!!document) this.ListDailyAssistance(true, document);
    });
  }

  ListDailyAssistance(filter: boolean, document: string = '')
  {
    this.attendance.DailyAssistance().subscribe( response => {
      this.spinner.show();
      this.AttendanceList = response.result;
      if(filter) this.AttendanceList = this.AttendanceList.filter( (item: any) => String(item.numerodocumento) === String(document));
      this.spinner.hide();
    }, (e) => this.fn.exception(e));
  }

}
