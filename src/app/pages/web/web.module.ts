import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebComponent } from './web.component';
import { RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ControlasistenciaComponent } from './controlasistencia/controlasistencia.component';
import { DemoMaterialModule } from 'src/app/materia-module';
import { ComponentsModule } from 'src/app/components/components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    WebComponent,
    InicioComponent,
    ControlasistenciaComponent


  ],
  imports: [
    CommonModule,
    RouterModule,
     SharedModule,
     HttpClientModule,
     AngularSvgIconModule.forRoot(),
     DemoMaterialModule,
     ComponentsModule,
     FormsModule,
     ReactiveFormsModule
  ]
})
export class WebModule { }
