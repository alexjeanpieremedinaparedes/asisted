const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
        content: [
            './src/**/*.{html,ts,css,scss,sass,less,styl}',
        ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {

        fontSize: {
            'xs': '.75rem',
            'ven': '.840rem',
            'sm': '.875rem',
            'tiny': '.875rem',
            'am': '0.932rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5rem',
        },


        borderWidth: {
            default: '1px',
            '0': '0',
            '1': '1px',
            '1.5': '0.11em',
            '2': '2px',
            '3': '3px',
            '4': '4px',
            '6': '6px',
            '8': '8px',
        },

        extend: {

            fontWeight: {
                hairline: 100,
                'extra-light': 100,
                thin: 200,
                light: 300,
                normal: 400,
                medium: 500,
                grande: 580,
                semibold: 600,
                masgrande: 605,
                bold: 700,
                extrabold: 800,
                'extra-bold': 800,
                black: 900,
            },

            width: {
                '1/7': '14.2857143%',
                '2/7': '28.5714286%',
                '3/7': '42.8571429%',
                '4/7': '57.1428571%',
                '5/7': '71.4285714%',
                '6/7': '85.7142857%',
                '33': '11rem',
                '40': '17.5rem',
                '83': '22rem',
                '97': '21rem',
                '98': '29rem',
                '99': '34.875rem',
                '99.4': '40.625',
                '100': '41.813rem',
                '110': '42rem',
                '120': '45rem',
                '130': '48rem',
                '140': '50rem'
            },

            height: {
                '82': '33rem',
                '95': '35rem',
                '100':'39rem',
                '110':'42rem'

            },

            colors: {

                'celeste': {
                    '100': '#E8F7FF',
                },

                'azul': {
                    '500': '#0D53FD',
                    '550': '#0044EA',
                    '700': '#072649',
                    '750': '#00263E',
                    '800': '#13385D',
                    '900': '#1D222E'
                },

                'plomo': {
                    '30': '#9DA3A7',
                    '40': '#F5F6FA',
                    '50': '#FBFBFB',
                    '60': '#A1A1A1',
                    '70': '#C4C4C4',
                    '80': '#E5E5E5',
                    '90': '#B9B9B9',
                    '100': '#BFBFBF',
                    '110': '#6F6F6F',
                    '120': '#5B5B5B',
                    '140': '#4F4E4E',
                    '150': '#DADDE5',
                    '160': '#7A7E81',
                    '180': '#78849F',
                    '200': '#EBEBEB',
                    '220':'#A6ADB0'
                },

                'rojo': {
                    '400': '#F74C57'
                },

            },

            fontFamily: {
                Roboto: ['Roboto'],
                Poppins: ['Poppins'],
                Manrope: ['Manrope']
            }

        },
    },
    variants: {
        extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/forms'), require('@tailwindcss/line-clamp'), require('@tailwindcss/typography')],
};